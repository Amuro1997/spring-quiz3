package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    String name;

    @Column
    Integer price;

    @Column
    String unit;

    @Column
    String url;

    public Product() {
    }

    public Product(String name, Integer price, String unit, String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }
}
