package com.twuc.webApp.domain;

import com.twuc.webApp.contract.PostOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductOrderRepo extends JpaRepository<ProductOrder, Integer> {
    Optional<ProductOrder> findByProductName(String name);
    void deleteByProductName(String name);
}
