package com.twuc.webApp.Service;

import com.twuc.webApp.contract.GetOrder;
import com.twuc.webApp.contract.PostOrder;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductOrder;
import com.twuc.webApp.domain.ProductOrderRepo;
import com.twuc.webApp.domain.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrderService {
    @Autowired
    ProductOrderRepo productOrderRepo;

    @Autowired
    ProductRepo productRepo;

    public void addOrder(PostOrder postOrder) {
        Optional<ProductOrder> byProductName = productOrderRepo.findByProductName(postOrder.getName());
        if(byProductName.isPresent()){
            byProductName.get().setQuantity(byProductName.get().getQuantity()+1);
            productOrderRepo.save(byProductName.get());
        }
        else productOrderRepo.save(new ProductOrder(postOrder.getName(), 1));
    }


    public List<GetOrder> getAllOrders() {
        return productOrderRepo.findAll().stream()
                .map(order->{
                    Product product = productRepo
                            .findByName(order.getProductName())
                            .orElseThrow(RuntimeException::new);
                    return new GetOrder(product.getName()
                            ,product.getPrice()
                            ,product.getUnit()
                            , order.getQuantity());
                })
                .collect(Collectors.toList());
    }

    public void removeOrder(String name) {
        productOrderRepo.deleteByProductName(name);
//        productOrderRepo.flush();
    }
}
