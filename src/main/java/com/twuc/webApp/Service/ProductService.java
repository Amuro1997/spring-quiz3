package com.twuc.webApp.Service;

import com.twuc.webApp.contract.PostProduct;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    ProductRepo productRepo;

    public void addProduct(PostProduct postProduct) {
        Product product = new Product(postProduct.getName(),
                postProduct.getPrice(), postProduct.getUnit(), postProduct.getUrl());
        productRepo.saveAndFlush(product);
    }

    public List<PostProduct> getAllProduct(){
        return productRepo.findAll().stream().map(
                (product->new PostProduct(product.getName(),product.getPrice(),product.getUnit(), product.getUrl()))
        ).collect(Collectors.toList());
    }
}
