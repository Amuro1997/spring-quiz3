package com.twuc.webApp.controller;

import com.twuc.webApp.Service.OrderService;
import com.twuc.webApp.Service.ProductService;
import com.twuc.webApp.contract.PostProduct;
import com.twuc.webApp.contract.PostOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class IndexController {

    @Autowired
    ProductService productService;

    @Autowired
    OrderService orderService;

    @PostMapping("/product")
    public ResponseEntity<?> createProduct(@RequestBody @Valid PostProduct postProduct){
        productService.addProduct(postProduct);
        return ResponseEntity.status(201).build();
    }

    @GetMapping("/product")
    public ResponseEntity getAllProduct(){
        return ResponseEntity.status(200).body(productService.getAllProduct());
    }

    @PostMapping("/order")
    public ResponseEntity<?> createOrder(@RequestBody @Valid PostOrder postOrder){
        orderService.addOrder(postOrder);
        return ResponseEntity.status(201).build();
    }

    @GetMapping("/order")
    public ResponseEntity<?> getAllOrder(){
        return ResponseEntity.status(200).body(orderService.getAllOrders());
    }

    @DeleteMapping("/order/{orderName}")
    public ResponseEntity<?> removeOrder(@PathVariable String orderName){
        orderService.removeOrder(orderName);
        return ResponseEntity.status(200).build();
    }
}
