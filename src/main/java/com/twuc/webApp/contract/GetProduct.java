package com.twuc.webApp.contract;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

public class GetProduct {
    Integer id;

    @NotNull
    String name;

    @NotNull
    Integer price;

    @NotNull
    String unit;

    @NotNull
    String url;

    public GetProduct() {
    }

    public GetProduct(String name, int price, String unit, String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
