package com.twuc.webApp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.PostOrder;
import com.twuc.webApp.contract.PostProduct;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureMockMvc
@Transactional
class ApiTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void shouldReturn201WhenAddAProduct() throws Exception {
        PostProduct postProduct = new PostProduct("AAA", 20, "f", "sdfds");
        String str = new ObjectMapper().writeValueAsString(postProduct);
        mockMvc.perform(post("https://localhost:8080/api/product")
            .contentType(MediaType.APPLICATION_JSON_UTF8).content(str))
                .andExpect(status().is(201));
    }

    @Test
    void shouldGetTheProduct() throws Exception {
        PostProduct postProduct = new PostProduct("AAA", 20, "unit", "sdfds");
        String str = new ObjectMapper().writeValueAsString(postProduct);
        mockMvc.perform(post("https://localhost:8080/api/product")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(str))
                .andExpect(status().is(201));

        mockMvc.perform(get("https://localhost:8080/api/product"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name", Matchers.is("AAA")));
    }

    @Test
    void shouldReturn201WhenCreateOrder() throws Exception {
        PostProduct postProduct = new PostProduct("AAA", 20, "unit", "sdfds");
        String str = new ObjectMapper().writeValueAsString(postProduct);
        mockMvc.perform(post("https://localhost:8080/api/product")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(str))
                .andExpect(status().is(201));

        PostOrder order = new PostOrder("AAA");
        str = new ObjectMapper().writeValueAsString(order);

        mockMvc.perform(post("https://localhost:8080/api/order")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(str))
                .andExpect(status().is(201));
    }

    @Test
    void shouldGetOrders() throws Exception {
        PostProduct postProduct = new PostProduct("AAA", 20, "unit", "sdfds");
        String str = new ObjectMapper().writeValueAsString(postProduct);
        mockMvc.perform(post("https://localhost:8080/api/product")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(str))
                .andExpect(status().is(201));

        PostOrder order = new PostOrder("AAA");
        str = new ObjectMapper().writeValueAsString(order);

        mockMvc.perform(post("https://localhost:8080/api/order")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(str))
                .andExpect(status().is(201));

        mockMvc.perform(get("https://localhost:8080/api/order"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name", Matchers.is("AAA")))
                .andExpect(jsonPath("$[0].quantity", Matchers.is(1)));
    }

    @Test
    void shouldRemoveOrder() throws Exception {
        PostProduct postProduct = new PostProduct("AAA", 20, "unit", "sdfds");
        String str = new ObjectMapper().writeValueAsString(postProduct);
        mockMvc.perform(post("https://localhost:8080/api/product")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(str))
                .andExpect(status().is(201));

        PostOrder order = new PostOrder("AAA");
        str = new ObjectMapper().writeValueAsString(order);

        mockMvc.perform(post("https://localhost:8080/api/order")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(str))
                .andExpect(status().is(201));


        mockMvc.perform(delete("https://localhost:8080/api/order/AAA"))
                .andExpect(status().is(200));

        mockMvc.perform(get("https://localhost:8080/api/order"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$", hasSize(0)));
    }
//
    @Test
    void shouldIncreaseQuantityWhenAddingSameItem() throws Exception {

        PostProduct postProduct = new PostProduct("SDf", 20, "unit", "sdfds");
        String str = new ObjectMapper().writeValueAsString(postProduct);
        mockMvc.perform(post("https://localhost:8080/api/product")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(str))
                .andExpect(status().is(201));

        PostOrder order = new PostOrder("SDf");
        str = new ObjectMapper().writeValueAsString(order);

        mockMvc.perform(post("https://localhost:8080/api/order")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(str))
                .andExpect(status().is(201));

        mockMvc.perform(post("https://localhost:8080/api/order")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(str))
                .andExpect(status().is(201));

        mockMvc.perform(get("https://localhost:8080/api/order"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].quantity", Matchers.is(2)));
    }
}
